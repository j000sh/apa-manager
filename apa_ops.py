"""
  Josh Lindsay
"""

from requests import Session
from bs4 import BeautifulSoup as BS
import re

URL = 'https://members.poolplayers.com'

DJ = 'DES_JSE'
DG = 'DES_Group'
VS = '__VIEWSTATE'
ET = '__EVENTTARGET'
EA = '__EVENTARGUMENT'
EV = '__EVENTVALIDATION'
VSG = '__VIEWSTATEGENERATOR'
LF = '__LASTFOCUS'
DSL = 'ddlSelectedLeague'
DDS = 'ddlDivisionSelector'

def get_soup(c):
    return BS(c, 'html.parser')

def do_event(s, r, target, extra = {}):
    data = get_event_data(r)
    data[ET] = target
    data.update(extra)
    #print('\n', target, r.url)
    #print(data.keys())
    r = s.post(r.url, data=data)
    #print('res', r.url)
    return r

def get_statistics(s, r):
    r = do_event(s, r, 'lbtnStatistics')
    soup = get_soup(r.content)

    # get current stats
    # get previous stats
    r = get_previous_session_stats(s, r)

    return r

def get_target(link):
    # javascript:__doPostBack('ctl00$cplhMainContent$rptPlayerHistory$ctl02$lbtnSpring','')
    return re.sub(r"javascript:__doPostBack\('([^']*)'.*", r'\1', link.get('href'))

def get_previous_session_stats(s, r):
    stats = {}
    r = do_event(s, r, 'ctl00$cplhMainContent$ctrlPlayerStatsNewControl$lbtnPreviousSessionStats')
    soup = get_soup(r.content)
    seasonlinks = soup.find_all(href=re.compile(r'ctl00\$cplhMainContent\$rptPlayerHistory'))
    for link in seasonlinks:
        t = get_target(link)
        get_session_stats(s, r, t)
    return r

def get_session_stats(s, r, target):
    stats = {}
    r = do_event(s, r, target)
    soup = get_soup(r.content)
    boxes = soup.find_all('div', attrs={'class': 'box'})
    for box in boxes:
        title = box.find('div', attrs={"class": "box_title"}).h1.text
        print(title)
        statsBoxes = box.find_all('table', attrs={"class": "stats"})
        for statsBox in statsBoxes:
            stats = parse_stats_box(statsBox)

#<span id="cplhMainContent_ctrlPlayerStatsNewControl_lblCurrentSession" class="bold_blue">Summer 2015</span>
def parse_stats_box(box):
    if not box.text.strip():
        return {}
    # teams
    if box.find(id='cplhMainContent_ctrlPlayerStatsNewControl_rptTeamStats_lblTeamNumber_0'):
        teams = get_team_stats(box)
    # player stats
    spans = [] #box.find_all('span', attrs={'class': 'blue_bold'})
    for span in spans:
        id = re.sub(r'cplhMainContent_ctrlPlayerStatsNewControl_(.*)', r'\1', span.get('id'))
        val = span.text
        stats[id] = span.text
    return {}

# working!
def get_team_stats(box):
    teams = []
    param = 'cplhMainContent_ctrlPlayerStatsNewControl_rptTeamStats_lbl'
    regex = re.compile(param + r'TeamNumber_*')
    num_teams = len(box.find_all(id=regex))
    for n in range(num_teams):
        team = {}
        regex = re.compile(param + r'.*_' + str(n))
        fields = box.find_all(id=regex)
        for field in fields:
            name = re.sub(param + r'(.*)_' + str(n), r'\1', field.get('id'))
            team[name] = field.text
        teams += team
    return teams

def get_division_standings(s, r):
    r = do_event(s, r, 'ctl00$cplhMainContent$ctrlPlayerStatsNewControl$lbtnDivisionStats')
    return r

def get_division_schedule(s, r):
    r = do_event(s, r, 'ctl00$cplhMainContent$ctrlDivisionStatsControl$lbtnDivisionSchedule')
    return r

def get_division_roster(s, r):
    r = do_event(s, r, 'ctl00$cplhMainContent$ctrlDivisionScheduleControl$lbtnDivisionRoster')
    return r

def get_event_data(r):
    soup = get_soup(r.content)
    data = {}

    def get_value(soup, id):
        val = soup.find(id=id)
        if val:
            return val['value']
        else:
            return ''

    # each request
    data[VS] = get_value(soup, VS)
    data[EV] = get_value(soup, EV)
    data[VSG] = get_value(soup, VSG)

    # always necessary?
    data[LF] = '' #get_value(soup, LF)
    data[EA] = '' #get_value(soup, EA)

    # elsewhere on page
    # data[DG] = '' #get_value(soup, DG)
    # data[DJ] = '' #get_value(soup, DJ)

    # TODO multiple divisions and leagues...
    data[DSL] = '2436652'
    data[DDS] = '220911'
    return data

def login(s, username, password):
    data = {}
    data['ctl00$cplhPublicContent$Login1$txtUserID'] = username
    data['ctl00$cplhPublicContent$Login1$txtPassword'] = password
    data['ctl00$cplhPublicContent$Login1$btnLogin'] = ''
    r = do_event(s, s.get(URL), '', data)
    print('logged in as', get_soup(r.content).find(id='lblFullName').text)
    return r

def logout(s, r):
    r = do_event(s, r, 'ctl00$lbtnLogout')
    if 'Default.aspx' in r.url:
        print('logged out')
    return r

